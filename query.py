import webapp2
import json
from google.appengine.ext import ndb

class tw_stock_etf (ndb.Model):
    id = ndb.StringProperty()


class tw_stock_data (ndb.Model):
    id = ndb.StringProperty()
    name = ndb.StringProperty()
    dy = ndb.FloatProperty()
    dy3 = ndb.FloatProperty()
    dy5 = ndb.FloatProperty()
    cdy = ndb.FloatProperty()
    cdy3 = ndb.FloatProperty()
    cdy5 = ndb.FloatProperty()
    ady = ndb.FloatProperty()
    ady3 = ndb.FloatProperty()
    ady5 = ndb.FloatProperty()
    td = ndb.FloatProperty()
    td3 = ndb.FloatProperty()
    td5 = ndb.FloatProperty()
    sd = ndb.FloatProperty()
    sd3 = ndb.FloatProperty()
    sd5 = ndb.FloatProperty()
    cd = ndb.FloatProperty()
    cd3 = ndb.FloatProperty()
    cd5 = ndb.FloatProperty()
    pt = ndb.FloatProperty()
    pt3 = ndb.FloatProperty()
    pt5 = ndb.FloatProperty()
    eps = ndb.FloatProperty()
    eps3 = ndb.FloatProperty()
    eps5 = ndb.FloatProperty()
    price = ndb.FloatProperty()
    roe = ndb.FloatProperty()
    per = ndb.FloatProperty()
    cdys = ndb.IntegerProperty()

    @classmethod
    def main_query(self, field, op, value):
        if not hasattr(tw_stock_data, field):
            return None

        try:
            fval = float(value)
        except:
            return None
        
        f = getattr(tw_stock_data, field)

        # FIXME: Generalize
        if field == 'cdys':
            fval = int(fval)

        if op == 'gt':
            return tw_stock_data.query().filter(f > fval).order(-f).fetch()
        elif op == 'ge':
            return tw_stock_data.query().filter(f >= fval).order(-f).fetch()
        elif op == 'eq':
            return tw_stock_data.query().filter(f == fval).order(-f).fetch()
        elif op == 'le':
            return tw_stock_data.query().filter(f <= fval).order(-f).fetch()

        return tw_stock_data.query().filter(f < fval).order(-f).fetch()
    
    def to_simple_dict(self):
        ret = {
            "id": self.id,
            "name": self.name,
            "dy": self.dy,
            "dy3": self.dy3,
            "dy5": self.dy5,
            "cdy": self.cdy,
            "cdy3": self.cdy3,
            "cdy5": self.cdy5,
            "ady": self.ady,
            "ady3": self.ady3,
            "ady5": self.ady5,
            "td": self.td,
            "td3": self.td3,
            "td5": self.td5,
            "cd": self.cd,
            "cd3": self.cd3,
            "cd5": self.cd5,
            "sd": self.sd,
            "sd3": self.sd3,
            "sd5": self.sd5,
            "pt": self.pt,
            "pt3": self.pt3,
            "pt5": self.pt5,
            "eps": self.eps,
            "eps3": self.eps3,
            "eps5": self.eps5,
            "roe": self.roe,
            "price": self.price,
            "per": self.per,
            "cdys": self.cdys
        }

        return ret


class RefineFilter:
    def __init__(self, filter_spec):
        self.filter_spec = filter_spec
    
    def __call__(self, item):
        try:
            itemvalue = getattr(item, self.filter_spec['field'])
            fval = float(self.filter_spec["value"])
            op = self.filter_spec["op"]
            if op == 'gt':
                return itemvalue > fval
            elif op == 'ge':
                return itemvalue >= fval
            elif op == 'eq':
                return itemvalue == fval
            elif op == 'le':
                return itemvalue <= fval

            return itemvalue < fval
        except:
            return False

class MainQuery(webapp2.RequestHandler):
    def post ( self ):
        self.response.headers['Content-Type'] = 'application/json'
        response_obj = { "results": [] }

        # Parse the JSON data from post data
        try:
            reqargs = json.loads(self.request.body)
        except:
            response_obj["error"] = 'Failed on parsing incoming JSON data from request.'

        etf_stockids = None
        if ('etf_only' in reqargs) and (True == reqargs['etf_only']):
            etf_stockids = set()
            results = tw_stock_etf.query().fetch()
            for r in results:
                etf_stockids.add(r.id)

        if len(reqargs['filters']) > 0:
            f = reqargs['filters'][0]
            results = tw_stock_data.main_query(f['field'], f['op'], f['value'])
            if None == results:
                response_obj["error"] = 'Internal error (tw_stock_data.make_query).'
            else:
                for qf in reqargs['filters'][1:]:
                    results = filter(RefineFilter(qf), results)

        for r in results:
            if (None != etf_stockids) and (r.id not in etf_stockids):
                continue
            response_obj['results'].append(r.to_simple_dict())

        self.response.write(json.dumps(response_obj))
            
    
app = webapp2.WSGIApplication([
    ('/query', MainQuery)
], debug = True)
