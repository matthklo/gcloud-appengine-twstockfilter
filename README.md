# 這是幹麻的?

* 台灣上市股票過濾器 [由此去](https://extended-arcana-202009.appspot.com/console.html)
* 基於 Google Cloud App Engine (Standard Python 環境)、以及 Google Cloud Datastore。兩者皆有免費額度，將程式碼 checkout 後即可運行在自己的 Google Cloud 帳號中。
* Google Cloud Datastore 中的股市資料是由另一個姊妹專案 [twstockfilter](https://github.com/matthklo/twstockfilter) 來收集、並且定期上傳。

# 開發環境備考

1. 安裝 [Google Cloud SDK](https://cloud.google.com/sdk/)
2. 透過 `gcloud init` 進行初始化
3. 於 Cloud SDK Shell 中在此目錄下執行 `dev_appserver.py app.yaml` 帶起測試環境
4. 訪問 http://localhost:8080 以測試
5. 訪問 http://localhost:8000 可觀察測試用的 admin console
6. 本地測試完成後，透過命令列指令 `gcloud app deploy` 可佈署到 Google Cloud App Engine 中

# 結構備考

1. `static` 目錄下是 App Engine 中的靜態網頁內容部分 (HTML, JS, CSS)
2. 網頁中會透過 XMLHttpRequest 呼叫 server 端 Web API，夾帶過濾條件並且預期返回符合條件的股票資訊 (以 JSON 格式傳遞)。該 Web API 則實作在 query.py 中。
3. query.py 內使用 ndb 向 Google Cloud Datastore 提取資料。