function build_field_selector()
{
    var s = document.createElement("select");
    s.setAttribute('name', 'field');

    var options_data = [ 
        ['dy', '殖利率 dy (%)'], 
        ['dy3', '三年平均殖利率 dy3 (%)'], 
        ['dy5', '五年平均殖利率 dy5 (%)'],
        ['cdy', '現金殖利率 cdy (%)'], 
        ['cdy3', '三年平均現金殖利率 cdy3 (%)'], 
        ['cdy5', '五年平均現金殖利率 cdy5 (%)'],
        ['pt', '稅後純益 pt (億)'], 
        ['pt3', '三年平均稅後純益 pt3 (億)'], 
        ['pt5', '五年平均稅後純益 pt5 (億)'],
        ['eps', 'EPS'], 
        ['eps3', '三年平均EPS'], 
        ['eps5', '五年平均EPS'],
        ['price', '成交價 price'],
        ['per', '本益比 per'],
        ['roe', '股東權益 roe (%)'],
        ['cdys', '已連續派發股利年數 cdys']
    ];

    for (var od in options_data)
    {
        var o = document.createElement('option');
        o.setAttribute('value', options_data[od][0]);
        o.innerHTML = options_data[od][1];
        s.appendChild(o);
    }

    return s;
}

function build_compare_selector()
{
    var s = document.createElement("select");
    s.setAttribute('name', 'compare');

    var options_data = [
        ['gt', '>'],
        ['ge', '>='],
        ['eq', '='],
        ['le', '<='],
        ['lt', '<']
    ];

    for (var od in options_data)
    {
        var o = document.createElement('option');
        o.setAttribute('value', options_data[od][0]);
        o.innerHTML = options_data[od][1];
        s.appendChild(o);
    }

    return s;
}

function build_query_argument_line( filter_setting, hasDeleteBtn )
{
    var argline = document.createElement("div");
    var ctl = build_field_selector();
    argline.appendChild(ctl);
    if (null != filter_setting)
    {
        ctl.value = filter_setting['field'];
    }

    ctl = build_compare_selector();
    argline.appendChild(ctl);
    if (null != filter_setting)
    {
        ctl.value = filter_setting['op'];
    }

    var i = document.createElement('input');
    i.setAttribute('type', 'number');
    i.setAttribute('name', 'val');
    if (null != filter_setting)
    {
        i.value = filter_setting['value'];
    }
    argline.appendChild(i);

    if (hasDeleteBtn)
    {
        var b = document.createElement('button');
        // Note: There can be multiple '-' buttons exist in the page.
        //       To know which '-' button generates the click  event,
        //       use addEventListener()  here  instead  of  assigning
        //       callback to 'onclick' property.
        b.setAttribute('type', 'button');
        b.addEventListener('click', remove_query_argument, false)
        b.innerHTML = '-';
        argline.appendChild(b);
    }

    return argline;
}

function append_empty_query_argument()
{
    var argroot = document.getElementById('query_args_root');
    if (argroot === null)
    {
        console.log('Error: element with id = "query_args_root" is missing.');
        return;
    }

    argroot.appendChild(
        build_query_argument_line(null, argroot.childElementCount > 0)
    );

    arrange_add_argument_button();
}

function append_query_argument(filters)
{
    var argroot = document.getElementById('query_args_root');
    if (argroot === null)
    {
        console.log('Error: element with id = "query_args_root" is missing.');
        return;
    }

    if ((filters === undefined) || (filters === null))
    {
        console.log('Error: append_query_argument: "filters" is either undefined or null.');
        return;
    }
    
    for (var idx = 0; idx < filters.length; ++idx)
    {
        argroot.appendChild(
            build_query_argument_line(filters[idx], argroot.childElementCount > 0)
        );
    }

    arrange_add_argument_button();
}

function remove_query_argument(evt)
{
    var div_to_remove = evt.target.parentElement;
    div_to_remove.parentElement.removeChild(div_to_remove);

    arrange_add_argument_button();
}

function arrange_add_argument_button()
{
    var argroot = document.getElementById('query_args_root');
    if (argroot === null)
        return;

    // Place the '+' button at the proper place.
    var btn = document.getElementById('append_arg_btn');
    if (btn === null)
    {
        // The '+' button has not yet created. Create one.
        btn = document.createElement('button');
        btn.id = 'append_arg_btn';
        btn.setAttribute('type', 'button');
        btn.onclick = append_empty_query_argument;
        btn.innerHTML = '+';
    }
    else
    {
        // Prepare for re-parent the button.
        btn.parentElement.removeChild(btn);
    }

    // Always place the '+' at the end of the last argument div.
    argroot.lastElementChild.appendChild(btn);
}

function init_page()
{
    // Try load the last used filter settings from local storage
    var filters = localStorage.getItem('filters');
    if (filters === null)
        append_empty_query_argument();
    else
        append_query_argument(JSON.parse(filters));

    var qbtn = document.getElementById('query_btn');
    qbtn.onclick = do_query;

    var etf_chk = localStorage.getItem('etf_only');
    if ((etf_chk != null) || (JSON.parse(etf_chk) === true))
    {
        var etfchk = document.getElementById('query_etf_only');
        etfchk.checked = true;
    }
}

function collect_post_data()
{
    var argroot = document.getElementById('query_args_root');
    var etfchk = document.getElementById('query_etf_only');
    if (argroot === null || etfchk === null)
    {
        return null;
    }

    var pdata = {};
    pdata["etf_only"] = etfchk.checked;

    var filters = [];
    sort_field = null; // This is a global variable.
    for (var idx = 0; idx < argroot.childNodes.length; ++idx)
    {
        var f = argroot.childNodes[idx];

        if (f.childNodes[2].value == "")
            continue;

        filters.push(
            { "field": f.childNodes[0].value, "op": f.childNodes[1].value, "value": f.childNodes[2].value }
        );

        if (filters.length == 1)
            sort_field = f.childNodes[0].value;
    }

    if (filters.length <= 0)
        return null;

    pdata["filters"] = filters;

    //console.log(JSON.stringify(pdata));
    return pdata;
}

function do_query()
{
    var pdata = collect_post_data();
    if (pdata === null)
    {
        show_error_result('Error: No filter.');
        return;
    }

    var req = new XMLHttpRequest();
    req.open('POST', 'query');
    req.onload = function() {
        clear_result();

        if (req.status == 200)
        {
            try
            {
                var response = JSON.parse(req.responseText);
                if ('error' in response)
                {
                    show_error_result(response['error']);
                }
                else if ('results' in response)
                {
                    show_result(response['results']);

                    // Save the last used filter settings in local storage
                    localStorage.setItem('filters', JSON.stringify(pdata["filters"]));
                    localStorage.setItem('etf_only', JSON.stringify(pdata["etf_only"]));
                }
                else
                {
                    show_error_result('Error: No "results" found in the response JSON.');
                }
            }
            catch(e)
            {
                show_error_result('Error: Faild on parsing JSON data from server response.');
            }
        }
        else
        {
            show_error_result('Error: ' + req.status + ' ' + req.statusText);
        }
    };

    req.send(JSON.stringify(pdata));
}

function show_result( data )
{
    var rroot = document.getElementById('result');

    if (data.length <= 0)
    {
        rroot.innerHTML = '(empty)';
        return;
    }

    var tablehtml = '<table>';

    // Build table header
    var header_text = [ 
        'id',
        'name',
        'price',
        'per',
        'dy', 
        'dy3', 
        'dy5',
        'cdy', 
        'cdy3', 
        'cdy5',
        'pt', 
        'pt3', 
        'pt5',
        'eps', 
        'eps3', 
        'eps5',
        'roe',
        'cdys'
    ];
    tablehtml += '<tr>';
    for (var idx = 0; idx < header_text.length; ++idx)
    {
        var field = header_text[idx];
        if (field == sort_field)
            tablehtml += ('<th class="sort_field">' + field + '</th>');
        else
            tablehtml += ('<th>' + field + '</th>');
    }
    tablehtml += '</tr>'

    for (var i = 0; i < data.length; ++i)
    {
        tablehtml += '<tr>';
        var stock_id = data[i]["id"];
        for (var idx = 0; idx < header_text.length; ++idx)
        {
            var field = header_text[idx];
            var value = data[i][field];

            if (field == 'id')
            {
                ;
            }
            else if (field == 'name')
            {
                var extlinks = 
                    '<a class="extlnk_kchart" href="https://tw.stock.yahoo.com/q/ta?s=' + stock_id + '" target="extlnk_kchart_' + stock_id + '">技</a>';
                extlinks +=
                    '<a class="extlnk_profit" href="https://goodinfo.tw/StockInfo/StockBzPerformance.asp?STOCK_ID=' + stock_id + '" target="extlnk_profit_' + stock_id + '">營</a>';
                value += extlinks;
            }
            else if (value === null)
            {
                value = 'n/a';
            }
            else
            {
                value = value.toFixed(2);
            }

            if (field == sort_field)
                tablehtml += ('<td class="sort_field">' + value + '</td>');
            else
                tablehtml += ('<td>' + value + '</td>');
        }
        tablehtml += '</tr>'
    }

    tablehtml += '</table>';
    rroot.innerHTML = tablehtml;
}

function show_error_result( errmsg )
{
    var rroot = document.getElementById('result');
    rroot.innerHTML = errmsg;
}

function clear_result()
{
    var rroot = document.getElementById('result');
    for (var idx = 0; idx < rroot.childNodes.length; idx++)
    {
        rroot.removeChild(rroot.childNodes[idx]);
    }
}

window.onload = init_page;